<?php
// +----------------------------------------------------------------------
// | HaoyundadaWordpress [ WE CAN DO IT JUST HAOYUNDADA ]
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind        : 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------

namespace haoyundada\facade;

use haoyundada\facade\Facade;

if (class_exists('haoyundada\Facade')) {
    class Facade extends \haoyundada\Facade
    {}
} else {
    class Facade
    {
        /**
         * 始终创建新的对象实例
         * @var bool
         */
        protected static $alwaysNewInstance;

        protected static $instance;

        /**
         * 获取当前Facade对应类名
         * @access protected
         * @return string
         */
        protected static function getFacadeClass()
        {}

        /**
         * 创建Facade实例
         * @static
         * @access protected
         * @return object
         */
        protected static function createFacade()
        {
            $class = static::getFacadeClass() ?: 'haoyundada\src\Template';

            if (static::$alwaysNewInstance) {
                return new $class();
            }

            if (!self::$instance) {
                self::$instance = new $class();
            }

            return self::$instance;

        }

        // 调用实际类的方法
        public static function __callStatic($method, $params)
        {
            return call_user_func_array([static::createFacade(), $method], $params);
        }
    }
}

/**
 * @see \haoyundada\src\Template
 * @mixin \haoyundada\src\Template
 */
class Template extends Facade
{
    protected static $alwaysNewInstance = true;

    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'haoyundada\src\Template';
    }
}
